import i18n from "i18next";
import { reactI18nextModule } from "react-i18next";
import English from './lang/en.json';
import Indonesian from './lang/id.json';

// the translations
const resources = {
  en: {
    translation: English
  },
  id: {
    translation: Indonesian
  }
};

i18n
  .use(reactI18nextModule) // passes i18n down to react-i18next
  .init({
    resources,
    fallbackLng: "en", // use en if detected lng is not available

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false // react already safes from xss
    }
  });

export default i18n;