import logo from './logo.svg';
import React from 'react';
import i18n from './i18next';
import './App.css';
import { withNamespaces } from 'react-i18next';

function App ({ t }) {
  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  }

  return (
    <div>
      <div class='App'>
        <button onClick={() => changeLanguage('id')}>Indonesian</button>
        <button onClick={() => changeLanguage('en')}>English</button>
      </div>

      <div class='App-header'>
        <img src={logo} className="App-logo" alt="logo" />

        <p>
          {t('Welcome to React')}
        </p>

        <p>
          {t('Tutorial')}
        </p>

        <a class='App-link' href='https://react.i18next.com/'>
          {t('Source')}
        </a>
      </div>
    </div>
  );
}

export default withNamespaces()(App)


// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

//export default App;
